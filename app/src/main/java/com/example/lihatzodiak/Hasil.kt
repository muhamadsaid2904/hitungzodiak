package com.example.lihatzodiak

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.lihatzodiak.databinding.HasilBinding
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

class Hasil: Fragment() {
    private lateinit var binding: HasilBinding
    private fun data() = (activity as MainActivity).data
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HasilBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLayoutListener()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setLayoutListener(){
        binding.apply {
            tvName.text = "Hallo ${data().nama}"
            val data = hitungUsia("${data().hari}-${data().bulan}-${data().tahun}")
            val zodiak = hitungZodiak("${data().hari}-${data().bulan}-${data().tahun}")
            tvUsia.text = "Usia anda saat ini adalah :"
            tvHasi.text = data
            Zodiak.text = "Bintang anda adalah\n" +
                    "$zodiak"

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun hitungUsia(tanggalLahir: String): String {
        return try {
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            val tanggalLahirDate = LocalDate.parse(tanggalLahir, formatter)
            val hariIni = LocalDate.now()

            val usiaPeriod = Period.between(tanggalLahirDate, hariIni)
            val tahun = usiaPeriod.years
            val bulan = usiaPeriod.months
            val hari = usiaPeriod.days

            "$tahun Tahun,\n$bulan Bulan,\n$hari Hari"
        } catch (e: DateTimeParseException) {
            "Format tanggal tidak valid"
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun hitungZodiak(tanggalLahir: String): String {
        try {

            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            val tanggalLahirDate = LocalDate.parse(tanggalLahir, formatter)

            return when {
                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 3, 21)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                4,
                                20
                            )
                        ) -> "Aries"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 4, 19)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                5,
                                21
                            )
                        ) -> "Taurus"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 5, 20)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                6,
                                21
                            )
                        ) -> "Gemini"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 6, 20)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                7,
                                23
                            )
                        ) -> "Cancer"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 7, 22)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                8,
                                23
                            )
                        ) -> "Leo"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 8, 22)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                9,
                                23
                            )
                        ) -> "Virgo"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 9, 22)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                10,
                                23
                            )
                        ) -> "Libra"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 10, 22)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                11,
                                22
                            )
                        ) -> "Scorpio"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 11, 21)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                12,
                                22
                            )
                        ) -> "Sagittarius"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 12, 21)) ||
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                1,
                                20
                            )
                        ) -> "Capricorn"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 1, 19)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                2,
                                19
                            )
                        ) -> "Aquarius"

                tanggalLahirDate.isAfter(LocalDate.of(tanggalLahirDate.year, 2, 18)) &&
                        tanggalLahirDate.isBefore(
                            LocalDate.of(
                                tanggalLahirDate.year,
                                3,
                                21
                            )
                        ) -> "Pisces"

                else -> "Zodiak tidak diketahui"
            }
        }catch (e: DateTimeParseException) {
            // Tangani kesalahan format tanggal di sini
            return "Format tanggal tidak valid"
        }
    }
}