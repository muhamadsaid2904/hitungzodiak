package com.example.lihatzodiak

import androidx.lifecycle.ViewModel

data class model (
    var nama:String = "",
    var hari:String = "",
    var bulan:String = "",
    var tahun:String = ""
): ViewModel()

